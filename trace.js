function trace(newTraceId) {
  return function middleware(req, res, next) {
    let traceId = req.get('X-API-Trace');

    if (!traceId) {
      traceId = newTraceId();
    }

    req.apiTrace = traceId;
    next();
  }
}

module.exports = trace;
