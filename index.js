const errors = require('./errors');
const response = require('./response');
const trace = require('./trace');

module.exports = {
  errors,
  response,
  trace
};
