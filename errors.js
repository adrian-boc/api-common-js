class AuthError extends Error {
    constructor(message, info) {
        super(message);
        this.info = info;
        Error.captureStackTrace(this, AuthError);
    }

    toJSON() {
      return {
        status: 403,
        errors: [ { type: 'AuthError', message: this.message, ref: [] } ]
      };
    }
}

class ValidationError extends Error {
    constructor(message, info) {
        super(message);
        this.info = info;
        Error.captureStackTrace(this, ValidationError);
    }

    toJSON() {
      return {
        status: 400,
        errors: [ { type: 'ValidationError', message: this.message, ref: this.info.ref } ]
      };
    }
}

class GroupValidationError extends Error {
    constructor(message, errors) {
        super(message);
        this.errors = errors;
        Error.captureStackTrace(this, GroupValidationError);
    }

    toJSON() {
      return {
        status: 400,
        errors: this.errors.map(error => ({ type: 'ValidationError', message: error.message, ref: error.ref }) )
      };
    }
}

class NotFoundError extends Error {
    constructor(message, info) {
        super(message);
        this.info = info;
        Error.captureStackTrace(this, NotFoundError);
    }

    toJSON() {
      return {
        status: 404,
        errors: [ { type: 'NotFoundError', message: this.message, ref: [] } ]
      };
    }
}

class CustomError extends Error {
    constructor(message, info) {
        super(message);
        this.info = info;
        Error.captureStackTrace(this, CustomError);
    }

    toJSON() {
      return {
        status: 500,
        errors: [ { type: 'Error', message: this.message, ref: [] } ]
      };
    }
}


module.exports = {
  AuthError,
  ValidationError,
  GroupValidationError,
  NotFoundError,
  CustomError
};
