// function errorEntry(errorType, message, ref) {
//   return { type: errorType, message, ref };
// }
//
// function buildJoiValidationErrorEntries(err) {
//   const status = 400;
//   const errors = err.details.map(detail => {
//     return errorEntry('ValidationError', detail.message, detail.path);
//   });
//   return { errors, status };
// }
//
// function buildValidationErrorEntries(err) {
//   const status = 400;
//   const errors = err.errors.map(error => {
//     return errorEntry('ValidationError', error.message, error.ref || []);
//   });
//   return { errors, status };
// }
//
// function buildSimpleErrorEntries(err) {
//   const status = 500;
//   const errors = [ errorEntry('Error', err.message, []) ];
//   return { errors, status };
// }
//
// function buildSequelizeUniqueConstraintErrorEntries(err) {
//   const status = 400;
//   const errors = err.errors.map(error => {
//     return errorEntry('ValidationError', `"${error.path}" must be unique`, [error.path]);
//   });
//   return { errors, status };
// }
//
// function buildError(err) {
//   let errors = [];
//   let status = 500;
//
//   if (err) {
//     let retval = { errors: [], status };
//     if (err.name === 'ValidationError') {
//       if (err.isJoi) {
//         retval = buildJoiValidationErrorEntries(err);
//       } else {
//         retval = buildValidationErrorEntries(err);
//       }
//     } else if (err.name === 'SequelizeUniqueConstraintError') {
//       retval = buildSequelizeUniqueConstraintErrorEntries(err);
//     } else if (err.name === 'SimpleError') {
//       retval = buildSimpleErrorEntries(err);
//     }
//     errors = retval.errors;
//     status = retval.status;
//   }
//
//   return {
//     result: "failure",
//     status,
//     errors
//   };
// }

function buildError(errors, status) {
  return {
    result: "failure",
    status,
    errors
  };
}

function buildSuccess(data, message, status) {
  return {
    result: "success",
    status,
    data,
    message
  };
}

// function buildErrorResponse(entries, status) {
//   const err = { status, errors: entries || [] };
//   return err;
// }
//
// function errorResponse(res, entries, status = 500) {
//   const response = buildErrorResponse(entries, status);
//   return res.status(response.status).json(response).end();
// }

function error(res, err) {
  const errorRes = Object.assign(buildError([], 500), err.toJSON());
  // const errorRes = Object.assign({ result: "failure", status: 500 }, err.toJSON());
  return res.status(errorRes.status).json(errorRes).end();
}

function success(res, data, message, status = 200) {
  const success = buildSuccess(data, message, status);
  return res.status(success.status).json(success).end();
}

module.exports = {
  // errorResponse,
  error,
  success
}
